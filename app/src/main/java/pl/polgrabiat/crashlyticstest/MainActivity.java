package pl.polgrabiat.crashlyticstest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Fabric fabric = new Fabric.Builder(this).debuggable(true).kits(new Crashlytics()).build();
    Fabric.with(fabric);
    setContentView(R.layout.activity_main);

    Button crashBtn = (Button) findViewById(R.id.crash);
    crashBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        throw new RuntimeException("FAKE BUG");
      }
    });
  }
}
